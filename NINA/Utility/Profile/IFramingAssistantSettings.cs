﻿namespace NINA.Utility.Profile {
    public interface IFramingAssistantSettings {
        int CameraHeight { get; set; }
        int CameraWidth { get; set; }
        double FieldOfView { get; set; }
    }
}