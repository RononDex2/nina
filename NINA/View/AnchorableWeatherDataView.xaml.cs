﻿using System.Windows.Controls;

namespace NINA.View {

    /// <summary>
    /// Interaction logic for AnchorableWeatherDataView.xaml
    /// </summary>
    public partial class AnchorableWeatherDataView : UserControl {

        public AnchorableWeatherDataView() {
            InitializeComponent();
        }
    }
}