﻿using System.Windows.Controls;

namespace NINA.View {

    /// <summary>
    /// Interaction logic for AnchorableCameraControlView.xaml
    /// </summary>
    public partial class AnchorableCameraControlView : UserControl {

        public AnchorableCameraControlView() {
            InitializeComponent();
        }
    }
}