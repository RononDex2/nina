﻿using System.Windows.Controls;

namespace NINA.View {

    /// <summary>
    /// Interaction logic for AnchorableFilterWheelView.xaml
    /// </summary>
    public partial class AnchorableFilterWheelView : UserControl {

        public AnchorableFilterWheelView() {
            InitializeComponent();
        }
    }
}